import dog from './image.jpg'
import './App.css';
import { useState } from 'react';

function MyTarget(props){
  if(props.hasImage){
    return (
      <div 
        className="image-box"
        id={props.id}
        onDragLeave={props.onDragLeave}
        onDrop={props.onDrop}
        onDragOver={props.onDragOver}
      >
        <img
          id="drag1"
          src={dog}
          draggable="true"
          onDragStart={(event) =>props.onDragStart(event, props.id)}
          width="150"
          height="150"
        />
      </div>
    );
  }else return ( <div 
    className="image-box"
    id={props.id}
    onDragLeave={props.onDragLeave}
    onDrop={props.onDrop}
    onDragOver={props.onDragOver}></div> )
    
}

function App() {
  let [targets, setTargets] = useState([{
    id:1,
    hasImage:true,
    hadImage:false
  },
  {
    id:2,
    hasImage:false,
    hadImage:false
  },
  {
    id:3,
    hasImage:false,
    hadImage:false
  },
  {
    id:4,
    hasImage:false,
    hadImage:false
  },
  {
    id:5,
    hasImage:false,
    hadImage:false
  }]);
//this event is called when click is mantained over the image
  let onDragStartHandler = (
    event, startId) => {
    event.dataTransfer.setDragImage(
      event.target, 
      event.pageX-event.target.offsetLeft, 
      event.pageY-event.target.offsetTop 
    )
    let newTargets = targets.map(target => {
      return (target.id === startId ? {...target, hadImage:true} : {...target, hadImage:false})
    })
    setTargets(newTargets);
  }
//called when a div detected an element over him
  let onDragOverHandler = (event) => {
    event.preventDefault();
    event.target.style.backgroundColor = "black";
    event.target.style.border = "5px dotted #aaaaaa";
  }

  let onDropHandler = (event) => {
    let count = 0;
    let newTargets = targets.map(target=>{
      if(target.id === Number(event.target.id))
        return {...target, hasImage:true}
      else{
        count++;
        return {...target, hasImage:false};
      }
    })
    //sending back the image to the origine
    if(count >= targets.length){
      newTargets = targets.map(target=>{
        if(target.hadImage)
          return {...target, hasImage:true}
        else return {...target, hasImage:false};
      })
    }
    setTargets(newTargets)
    
    //css code 
    event.target.style.opacity = "1";
    event.target.style.border = "1px solid #aaaaaa";
    let divs = document.querySelectorAll('.image-box');
    for(let i =0 ; i < divs.length; i++)
      divs[i].style.backgroundColor = "white";
  }


  let style = {
    width: '350px',
    height: '70px',
    padding: '10px',
    border: '1px solid #aaaaaa'
  }

  let onDragLeaveHandler = (event) =>{
    let divs = document.querySelectorAll('.image-box');
    for(let i =0 ; i < divs.length; i++)
      divs[i].style.backgroundColor = "white";
    event.target.style.border = "1px solid #aaaaaa";
  }

  return (
    <div className="App">
      {
        targets.map(
          target =>
          <MyTarget
            key={target.id}
            id={target.id}
            onDragLeave={onDragLeaveHandler}
            onDrop={onDropHandler} style={style}
            onDragOver={onDragOverHandler}
            hasImage={target.hasImage}
            onDragStart={onDragStartHandler}
          />
        )
      }  
        
    
    </div>
  );
}

export default App;
